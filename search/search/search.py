# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    startPoint = problem.getStartState()
    c = problem.getStartState()
    alreadyVisited = [startPoint]
    states = util.Stack()
    states.push((startPoint, []))
    while not states.isEmpty() and not problem.isGoalState(c):
        state, actions = states.pop()
        alreadyVisited.append(state)
        successor = problem.getSuccessors(state)
        for i in successor:
            coordinates = i[0]
            if coordinates not in alreadyVisited:
                c = i[0]
                directions = i[1]
                print(directions)
                states.push((coordinates, actions + [directions]))

    return actions + [directions]


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"

    from util import Queue

    states = Queue()
    states.push((problem.getStartState(), []))
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                newPath = path + [direction]
                states.push((item[0], newPath))
        currentState, path = states.pop()
    return path

def uniformCostSearch(problem):
    from util import PriorityQueue
    states = PriorityQueue()
    states.push((problem.getStartState(), []),0)
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                newPath = path + [direction]
                cost=problem.getCostOfActions(newPath)
                states.push((item[0], newPath),cost)
        currentState, path = states.pop()
    return path

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    from util import PriorityQueue
    states = PriorityQueue()
    states.push((problem.getStartState(), []), 0)
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                succ=item[0]
                newPath = path + [direction]
                cost = problem.getCostOfActions(newPath)+heuristic(succ,problem)
                states.push((item[0], newPath), cost)
        currentState, path = states.pop()
    return path
def firstChoiceHillClimbing(problem):
    import random
    current_state=problem.getStartState()
    moves=[]
    successors=[]
    while(not problem.isGoalState(current_state)):
      successors=problem.getSuccessors(current_state)
      random.shuffle(successors)
      current_state=successors[0][0]
      moves.append(successors[0][1])
    return moves
def steepestAscentHillClimb(problem,heuristic=nullHeuristic):
    current_state=problem.getStartState()
    moves=[]
    successsors=[]
    visitedNodes=[]
    while not problem.isGoalState(current_state):
        visitedNodes.append(current_state)
        successors=problem.getSuccessors(current_state)
        min=9999
        for state,move,cost in successors :
            if heuristic(state,problem) <min and state not in visitedNodes:
                print("state : "+str(state))
                nextState=state
                nextMove=move
                nextCost=cost
                min=heuristic(state,problem)
        current_state=nextState
        moves.append(nextMove)
    return moves

def limitedDepthFirstSearch(problem):
    from util import Stack

    visitedNodes = []
    moves = []
    stackForMoves = Stack()
    depth = 0
    depth_limit = 100
    if problem.isGoalState(problem.getStartState()):
        return []

    stackForMoves.push((problem.getStartState(), moves))

    # x= len(stackForMoves)
    while not stackForMoves.isEmpty():
        if (depth <= depth_limit):
            top, moves = stackForMoves.pop()
            visitedNodes.append(top)

            if problem.isGoalState(top):
                return moves
            else:
                nextStep = problem.getSuccessors(top)
            for i in nextStep:
                if i[0] not in visitedNodes:
                    newPath = moves + [i[1]]
                    stackForMoves.push((i[0], newPath))
                    depth = depth + 1
        else:
            print("MAX LIMIT")
            return []
        # x = len(stackForMoves)

    return []


def anytimeAStar(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "* YOUR CODE HERE *"
    from util import PriorityQueue
    queueForMoves = PriorityQueue()
    queueForMoves.push((problem.getStartState(), []), 0)
    visitedNodes = []
    moves = []
    w = 3
    while not queueForMoves.isEmpty():
        top, moves = queueForMoves.pop()
        if problem.isGoalState(top):
            return moves
        else:
            nextStep = problem.getSuccessors(top)
        if nextStep:
            for item in nextStep:
                if item[0] not in visitedNodes:
                    newPath = moves + [item[1]]
                    visitedNodes.append(item[0])
                    queueForMoves.push((item[0], newPath), item[2] + w * heuristic(item[0], problem))
def iterativeDeepeningAstar(problem,heuristic=nullHeuristic):
    from util import Stack

    states = Stack()
    states.push((problem.getStartState(), []))
    bound=heuristic(problem.getStartState(),problem)
    min=99999
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            function=problem.getCostOfActions(path)+heuristic(currentState,problem)
            if bound >= function:
                print ("current state :"+str(currentState)+ " has function : " + str(function))
                print ("Bound right now is : " + str(bound))
                successors = problem.getSuccessors(currentState)
                for item in successors:
                    direction = item[1]
                    newPath = path + [direction]
                    states.push((item[0], newPath))
            else:
                if function < min:
                    min=function
        if min != 99999:
            bound =min
            print(" NEW BOUND : "+str(bound))
        currentState, path = states.pop()
        if states.isEmpty():
            print(" PUZZLE CANT BE RESOLVED WITH THIS ALGORITHM")
            return []
    return path






bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
fch = firstChoiceHillClimbing
ldfs = limitedDepthFirstSearch
aastar = anytimeAStar
ida = iterativeDeepeningAstar
sahc=steepestAscentHillClimb
# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    startPoint = problem.getStartState()
    c = problem.getStartState()
    alreadyVisited = [startPoint]
    states = util.Stack()
    states.push((startPoint, []))
    while not states.isEmpty() and not problem.isGoalState(c):
        state, actions = states.pop()
        alreadyVisited.append(state)
        successor = problem.getSuccessors(state)
        for i in successor:
            coordinates = i[0]
            if coordinates not in alreadyVisited:
                c = i[0]
                directions = i[1]
                print(directions)
                states.push((coordinates, actions + [directions]))

    return actions + [directions]


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"

    from util import Queue

    states = Queue()
    states.push((problem.getStartState(), []))
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                newPath = path + [direction]
                states.push((item[0], newPath))
        currentState, path = states.pop()
    return path

def uniformCostSearch(problem):
    from util import PriorityQueue
    states = PriorityQueue()
    states.push((problem.getStartState(), []),0)
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                newPath = path + [direction]
                cost=problem.getCostOfActions(newPath)
                states.push((item[0], newPath),cost)
        currentState, path = states.pop()
    return path

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    from util import PriorityQueue
    states = PriorityQueue()
    states.push((problem.getStartState(), []), 0)
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            for item in successors:
                direction = item[1]
                succ=item[0]
                newPath = path + [direction]
                cost = problem.getCostOfActions(newPath)+heuristic(succ,problem)
                states.push((item[0], newPath), cost)
        currentState, path = states.pop()
    return path


def stochasticHillClimbing(problem,heuristic=nullHeuristic):
    import random
    current_state=problem.getStartState()
    moves=[]
    successors=[]
    min=9999
    while(not problem.isGoalState(current_state)):
        if(heuristic(current_state,problem)<=min):
            successors=problem.getSuccessors(current_state)
            random.shuffle(successors)
            current_state=successors[0][0]
            moves.append(successors[0][1])
            min=heuristic(current_state,problem)
    return moves

def steepestAscentHillClimb(problem,heuristic=nullHeuristic):
    current_state=problem.getStartState()
    moves=[]
    successsors=[]
    visitedNodes=[]
    while not problem.isGoalState(current_state):
        visitedNodes.append(current_state)
        successors=problem.getSuccessors(current_state)
        min=9999
        for state,move,cost in successors :
            if heuristic(state,problem) <min and state not in visitedNodes:
                print("state : "+str(state))
                nextState=state
                nextMove=move
                nextCost=cost
                min=heuristic(state,problem)
        if(nextState==current_state):
            import random
            random.shuffle(successors)
            nextState= successors[0][0]
            nextMove = successors[0][1]
        current_state=nextState
        moves.append(nextMove)
    return moves

def limitedDepthFirstSearch(problem):
    from util import Stack

    visitedNodes = []
    moves = []
    stackForMoves = Stack()
    depth = 0
    depth_limit = 100
    if problem.isGoalState(problem.getStartState()):
        return []

    stackForMoves.push((problem.getStartState(), moves))

    # x= len(stackForMoves)
    while not stackForMoves.isEmpty():
        if (depth <= depth_limit):
            top, moves = stackForMoves.pop()
            visitedNodes.append(top)

            if problem.isGoalState(top):
                return moves
            else:
                nextStep = problem.getSuccessors(top)
            for i in nextStep:
                if i[0] not in visitedNodes:
                    newPath = moves + [i[1]]
                    stackForMoves.push((i[0], newPath))
                    depth = depth + 1
        else:
            print("MAX LIMIT")
            return []
        # x = len(stackForMoves)

    return []


def anytimeAStar(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "* YOUR CODE HERE *"
    from util import PriorityQueue
    queueForMoves = PriorityQueue()
    queueForMoves.push((problem.getStartState(), []), 0)
    visitedNodes = []
    moves = []
    w = 1000
    while not queueForMoves.isEmpty():
        top, moves = queueForMoves.pop()
        if problem.isGoalState(top):
            return moves
        else:
            nextStep = problem.getSuccessors(top)
        if nextStep:
            for item in nextStep:
                if item[0] not in visitedNodes:
                    newPath = moves + [item[1]]
                    visitedNodes.append(item[0])
                    queueForMoves.push((item[0], newPath), item[2] + w * heuristic(item[0], problem))


def iterativeDeepeningAstar(problem,heuristic=nullHeuristic):
    from util import Stack
    import random

    states = Stack()
    states.push((problem.getStartState(), []))
    bound=99999
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        counter =0
        
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            random.shuffle(successors)
            for item in successors:
                direction = item[1]
                newPath = path + [direction]
                print("hei")
                if(bound>=problem.getCostOfActions(newPath)+heuristic(item[0],problem)):
                    states.push((item[0], newPath))
                    counter=counter+1
                    print(counter)
            if counter == 0:
                bound=99999    
                for item in successors:
                    direction = item[1]
                    newPath = path + [direction]
                    if(bound<problem.getCostOfActions(newPath)+heuristic(item[0],problem)):
                        states.push((item[0], newPath))
                     
          
        if states.isEmpty():
            print(" PUZZLE CANT BE RESOLVED WITH THIS ALGORITHM")
            return []
        currentState, path = states.pop()
        
    return path

def newCost (direction):
   
    if direction == "South":
        return 2
    if direction == "West":
        return 3
    if direction == "East":
        return 4
    if direction == "North":
        return 1
    
  
def firstHillClimbingSearch(problem,heuristic=nullHeuristic):
    current_state = problem.getStartState()
    
    successors = []
    actions = []
    visitedNodes = []
    print(current_state)
    while (problem.isGoalState(current_state) == False):
        state=current_state
        successors = problem.getSuccessors(current_state)
        import random
        random.shuffle(successors)
        for item in successors:
            action=item[1]
            if(heuristic(item[0],problem)+problem.getCostOfActions(actions+[action])<heuristic(current_state,problem)+problem.getCostOfActions(actions)):
               # if(item[0] not in visitedNodes):
                    visitedNodes.append(item[0])
                    current_state = item[0]
                    actions.append(action)
                    break
        if(current_state==state):
           # if(item[0] not in visitedNodes):
                visitedNodes.append(successors[0][0])
                current_state=successors[0][0]
                actions.append(successors[0][1])
        print(current_state)
        
        

    return actions

def enforcedHillClimbSearch(problem,heuristic=nullHeuristic):
    from util import Queue
    import random
    states=Queue()
    states.push((problem.getStartState(),[]))
    best_heuristic=heuristic(problem.getStartState(),problem)
    while not states.isEmpty():
        current_state,moves=states.pop()
        print("Current state :"+str(current_state))
        successors=problem.getSuccessors(current_state)
        random.shuffle(successors)
        for item in successors :
            next_state=item[0]
            direction=item[1]
            local_heuristic=heuristic(next_state,problem)
            if problem.isGoalState(next_state):
                return moves+[direction]
            if local_heuristic < best_heuristic:
                best_heuristic=local_heuristic
                while not states.isEmpty():
                    states.pop()
                break
        moves = moves + [direction]
        states.push((next_state, moves))
    return moves
def beamSearch(problem,heuristic=nullHeuristic):
    from util import Queue
    beam =3

    states = Queue()
    states.push((problem.getStartState(), []))
    visitedNodes = []
    currentState, path = states.pop()
    while not problem.isGoalState(currentState):
        counter=0
        if currentState not in visitedNodes:
            visitedNodes.append(currentState)
            successors = problem.getSuccessors(currentState)
            successors2=[]
            for item in successors:
                itemHeuristic=heuristic(item[0],problem)
                successors2.append((item[0],item[1],item[2],itemHeuristic))
            successors2.sort(key=lambda x: x[3])
            for item in successors2:
                if counter<beam:
                    direction = item[1]
                    newPath = path + [direction]
                    states.push((item[0], newPath))
                    counter+=1
                else:
                    break
        currentState, path = states.pop()
    return path


bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
###
shc= stochasticHillClimbing#merge pe tot
ldfs = limitedDepthFirstSearch#merge pe tot
aastar = anytimeAStar#merge pe tot
ida = iterativeDeepeningAstar#merge pe big
sahc=steepestAscentHillClimb#merge pe tot
fhcs = firstHillClimbingSearch#dureaza foarte mult pe medium si pe big
ehcs=enforcedHillClimbSearch#merge pe tot
bs=beamSearch#2 succesori merpe pe tiny , 3 succesori merge pe tot